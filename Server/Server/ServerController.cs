﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Server.Client;

namespace Server;

public class ServerController
{
    public static int MaxPlayer;
    public static int Port;

    private static TcpListener tcpListener;

    private static readonly Dictionary<int, Client.Client> Clients = new Dictionary<int, Client.Client>();

    public static void Start(int maxPlayer, int port)
    {
        MaxPlayer = maxPlayer;
        Port = port;

        Console.WriteLine($"Server is starting on Port {port}");

        InitializeClients();

        tcpListener = new TcpListener(IPAddress.Any, port);
        tcpListener.Start();
        tcpListener.BeginAcceptTcpClient(new AsyncCallback(RecieveClinetConnection), null);


        Console.WriteLine($"Server is UP on Port {port}!");
    }


    private static void RecieveClinetConnection(IAsyncResult result)
    {
        TcpClient client = tcpListener.EndAcceptTcpClient(result);

        tcpListener.BeginAcceptTcpClient(new AsyncCallback(RecieveClinetConnection), null);

        Console.WriteLine($"Player connect from {client.Client.RemoteEndPoint}");

        for (int i = 1; i <= MaxPlayer; i++)
        {
            if (Clients[i].TcpClient.Socket == null)
            {
                Clients[i].TcpClient.Connect(client);
                return;
            }
        }
    }


    private static void InitializeClients()
    {
        for (int i = 1; i <= MaxPlayer; i++)
        {
            Clients.Add(i, new Client.Client(i));
        }
    }
}

