﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Server.Client;

public class Client
{
    public int id;
    public SocketTcp TcpClient;

    public Client(int id)
    {
        this.id = id;
        TcpClient = new SocketTcp(id);
    }
}

