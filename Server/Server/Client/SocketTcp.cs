﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;

namespace Server.Client;

public class SocketTcp
{

    private const int bufferDataSize = 4096;

    public TcpClient Socket;

    public NetworkStream Stream;
    public byte[] RecieveBuffer;

    private readonly int id;

    public SocketTcp(int id)
    {
        this.id = id;
    }

    public async void Connect(TcpClient socket)
    {
        Socket = socket;
        Socket.ReceiveBufferSize = bufferDataSize;
        Socket.SendBufferSize = bufferDataSize;
        RecieveBuffer = new byte[bufferDataSize];
        WaitForRecieveData();
    }

    private async void WaitForRecieveData()
    {
        Stream = Socket.GetStream();
        //Stream.BeginRead(RecieveBuffer, 0, RecieveBuffer.Length, ReciveData, null);
        var recieveDataLenth = await Stream.ReadAsync(RecieveBuffer, 0, RecieveBuffer.Length);
        ReceiveDataAsync(RecieveBuffer.Skip(0).Take(recieveDataLenth).ToArray());
    }

    //private void ReciveData(IAsyncResult result)
    //{
    //    try
    //    {
    //        int byteLenth = Stream.EndRead(result);
    //        if(byteLenth == 0)
    //        {
    //            //TODO: Disconnect
    //            return;
    //        }
    //
    //        byte[] data = new byte[byteLenth];
    //        Array.Copy(RecieveBuffer, data, byteLenth);
    //        //TODO: Handle Data
    //
    //        Stream.BeginRead(RecieveBuffer, 0, RecieveBuffer.Length, ReciveData, null);   
    //    }
    //    catch (Exception e)
    //    {
    //        Console.WriteLine(e.Message);
    //
    //        //TOOD: Disconnect
    //    }
    //}

    private void ReceiveDataAsync(byte[] data)
    {
        //jndaisdjs

        WaitForRecieveData();
    }



}

